# Makefile for tinydtls
#
#
# Copyright (c) 2011, 2012, 2013, 2014, 2015, 2016 Olaf Bergmann (TZI) and others.
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# and Eclipse Distribution License v. 1.0 which accompanies this distribution.
#
# The Eclipse Public License is available at http://www.eclipse.org/legal/epl-v10.html
# and the Eclipse Distribution License is available at
# http://www.eclipse.org/org/documents/edl-v10.php.
#
# Contributors:
#    Olaf Bergmann  - initial API and implementation
#

# the library's version
VERSION:=0.8.6

DTLS_SUPPORT   ?= posix
LOG_LEVEL_DTLS ?= LOG_LEVEL_INFO

KLEE_BUILD_PATH=/home/hooman/Repos/symbolicexecution/klee/build
KLEE_HEADER_PATH=/home/hooman/Repos/symbolicexecution/klee/include


# files and flags

LIB:=libtinydtls.a 

LDFLAGS := -L.. -L.
LDLIBS  := -ldummy-libklee

ARFLAGS:=cru
doc:=doc

ifdef cov
COV = -fprofile-arcs -ftest-coverage
else
COV =
endif

ifdef sym
SYM = -Xclang -disable-llvm-passes -D__NO_STRING_INLINES  -D_FORTIFY_SOURCE=0 -U__OPTIMIZE__
else
SYM =
endif

SOURCES = dtls.c dtls-crypto.c dtls-ccm.c dtls-hmac.c netq.c dtls-peer.c
SOURCES+= dtls-log.c
SOURCES+= aes/rijndael.c ecc/ecc.c sha2/sha2.c $(DTLS_SUPPORT)/dtls-support.c
SOURCES+= symbolic.c
SOURCES+= records.c


OBJECTS:= $(SOURCES:.c=.o)

CFLAGS:=-DLOG_LEVEL_DTLS=$(LOG_LEVEL_DTLS) -Wall -std=c99 -g -O0 $(COV) $(SYM) -I. -I$(DTLS_SUPPORT) -I $(KLEE_HEADER_PATH)



.PHONY: all clean doc

.SUFFIXES:
.SUFFIXES:      .c .o

all:	$(LIB)

check:
	$(MAKE) -C tests check

$(LIB):	$(OBJECTS)
	$(AR) $(ARFLAGS) $@ $^ 
	ranlib $@

SUBDIRS:=tests tests/unit-tests doc platform-specific sha2 aes ecc

clean:
	@rm -f $(LIB) $(OBJECTS)
	@rm -f *.gcno
	@rm -f *.gcda
	@rm -f *.gcov
	@rm -f aes/*.gcda
	@rm -f aes/*.gcno
	@rm -f ecc/*.gcda
	@rm -f ecc/*.gcno
	@rm -f sha2/*.gcda
	@rm -f sha2/*.gcno
	@rm -f posix/*.gcda
	@rm -f posix/*.gcno
	
	for dir in $(SUBDIRS); do \
		rm -f $$dir/.*.o.bc ;\
		rm -f .*.o.bc ;\
	done

doc:
	$(MAKE) -C doc
