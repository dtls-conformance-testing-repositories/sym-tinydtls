import argparse
import os
import subprocess

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", dest="input_dir", help="input directory containing input testcases", required=True)
    parser.add_argument("-p", dest="app_dir", help="address of the program", required=True)
    parser.add_argument("-c", dest="cmd", help="command to be executed on the program(use @@ to specify testcase "
                                               "placeholder)", required=True)
    return parser.parse_args()


def main():
    p = parse_args()
    inp_addr = os.path.abspath(p.input_dir)
    app_addr = os.path.abspath(p.app_dir)
    command = p.cmd
    with os.scandir(inp_addr) as entries:
        for entry in entries:
            if entry.is_file():
                try:
                    temp = str(p.cmd).replace("@@", os.path.join(inp_addr, entry.name))
                    args = temp.split(" ")
                    args[0] = args[0].replace("./", "")
                    args[0] = os.path.join(app_addr, args[0])
                    pr = subprocess.check_output(args)
                    print(pr)
                except:
                    print(args)



if __name__ == "__main__":
    main()
