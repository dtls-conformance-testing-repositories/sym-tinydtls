readonly SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
readonly CLIENT_DIR="$SCRIPT_DIR/client"
readonly SERVER_DIR="$SCRIPT_DIR/server"

readonly CTINYDTLS='ctinydtls'
readonly CTINYDTLS_REP_URL='https://github.com/contiki-ng/tinydtls.git'
readonly CTINYDTLS_COMMIT='53a0d97'
readonly CTINYDTLS_PATCH="ctinydtls.patch"
readonly TLSATTACKER_VER="3.0b"
readonly TLSATTACKER_FULLNAME="TLS-Attacker-$TLSATTACKER_VER"
readonly TLSATTACKER_ARCH_URL="https://github.com/RUB-NDS/TLS-Attacker/archive/$TLSATTACKER_VER.tar.gz"


function clone_rep() {
    sut_dir=$1
    rep_url=$2
    commit_num=$3
    
    echo "Cloning repository $rep_url  to $sut_dir"
    git clone $rep_url $sut_dir
    ( cd $sut_dir ; git checkout $commit_num )
}

# downloads and unpacks and archive
function solve_arch() {
    arch_url=$1
    target_dir=$2
    temp_dir=/tmp/`(basename $arch_url)`
    echo $temp_dir
    echo "Fetching/unpacking from $arch_url into $target_dir"
    if [[ ! -f "$temp_dir" ]]
    then
        echo "Downloading archive from url to $temp_dir"
        wget -nc --no-check-certificate $arch_url -O $temp_dir
    fi
    
    mkdir $target_dir
    # ${temp_dir##*.} retrieves the substring between the last index of . and the end of $temp_dir
    arch=`echo "${temp_dir##*.}"`
    if [[ $arch == "xz" ]]
    then
        tar_param="-xJf"
    else 
        tar_param="zxvf"
    fi
    echo $tar_param
    if [ $target_dir ] ; then
        tar $tar_param $temp_dir -C $target_dir --strip-components=1
    else 
        tar $tar_param $temp_dir
    fi
}

# Applies patches for SUTs that require them
function apply_patch() {
    sut_dir=$1
    sut_patch=$2
    echo "Applying patch $sut_patch via git apply"
    ( cd $sut_dir; git apply ../$sut_patch )
}


# builds tinydtls
function make_tinydtls() {
    sut_dir=$1
    ( 
        cd $sut_dir/tests 
        make clean all
    )
}

function setup_tlsattacker() {
    if [[ ! -d $TLSATTACKER_FULLNAME ]]
    then 
        solve_arch $TLSATTACKER_ARCH_URL $TLSATTACKER_FULLNAME
        ( cd $TLSATTACKER_FULLNAME; mvn install -DskipTests )
    fi
}

function setup_tinydtls() {
    if [[ ! -d $CTINYDTLS ]]
    then 
        clone_rep $CTINYDTLS $CTINYDTLS_REP_URL $CTINYDTLS_COMMIT
        apply_patch $CTINYDTLS $CTINYDTLS_PATCH
    fi
    if [[ ! -f $CTINYDTLS/tests/dtls-server ]]
    then 
        make_tinydtls $CTINYDTLS
    fi
}

function reproduce_server() {
    server_workflow=$1
    server_log=$SCRIPT_DIR/.server_log
    
    echo "" > $server_log

    # runs TinyDTLS server 
    ( cd $CTINYDTLS; ./tests/dtls-server -p 20220 ) &

    # runs TLS-Attacker's TLS-Client utility to execute the workflow as a client
    java -jar $TLSATTACKER_FULLNAME/apps/TLS-Client.jar -connect localhost:20220 -version DTLS12 -workflow_input $server_workflow  -config tlsattacker_server.config | tee $server_log

    # kills leftover processes
    kill $!
    bash $SCRIPT_DIR/stop_proc_at_port.sh 20220 both

    has_data=$(grep "Received Messages (client): APPLICATION" $server_log --count)
    if [[ $has_data -gt 0 ]]; then
        echo "A handshake has been completed successfully (received application data)"
    else 
        echo "Could not complete handshake"
    fi
}

function reproduce_client() {
    client_workflow=$1
    client_log=$SCRIPT_DIR/.client_log

    echo "" > $client_log

    # runs TinyDTLS client after a brief delay
    ( sleep 20; cd $CTINYDTLS; ./tests/dtls-client localhost 20220 ) &

    # runs TLS-Attacker's TLS-Server utility to execute the workflow as a server
    java -jar $TLSATTACKER_FULLNAME/apps/TLS-Server.jar -port 20220 -version DTLS12 -workflow_input $client_workflow  -config tlsattacker_client.config | tee $client_log

    # kills leftover processes
    kill $!
    bash $SCRIPT_DIR/stop_proc_at_port.sh 20220 both

    has_data=$(grep "Received Messages (server): APPLICATION" $client_log --count)
    if [[ $has_data -gt 0 ]]; then
        echo "A handshake has been completed successfully (received application data)"
    else 
        echo "Could not complete handshake"
    fi
}

if [[ ! $# -eq 1 ]]; then
    echo "Usage: reproduce.sh workflow"
    echo "Where workflow is an .xml corresponding to a rule, located in $CLIENT_DIR or $SERVER_DIR directories. "
    exit
fi

if [[ ! -f $1 ]]; then
    echo "Workflow isn't a file"
    exit
fi 

wfpath=$(realpath $1)
wfparent=$(dirname $wfpath)

if [[ $wfparent != $CLIENT_DIR && $wfparent != $SERVER_DIR ]]; then
    echo "Workflow should be a file in either $CLIENT_DIR or $SERVER_DIR"
    exit
fi

is_client=0
if [[ $wfparent == $CLIENT_DIR ]]; then
    is_client=1
fi

cd $SCRIPT_DIR

# setting up TLS-Attacker and Contiki-ng TinyDTLS 
setup_tinydtls
setup_tlsattacker

if [[ $is_client -eq 0 ]]; then
    reproduce_server $1
else 
    reproduce_client $1
fi
