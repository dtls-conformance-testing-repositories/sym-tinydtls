This directory contains witness workflows (i.e. sequences of send/receive record actions) for each of the rules that were found to be violated.
By executing these workflows, we can confirm the presence of the non-conforming behavior in the DTLS client/server applications.
This step is necessary to rule out false positives, that is, rules that were only broken due to our harness.

