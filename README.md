TinyDTLS^C
==============================
In the following, you can find the TinyDTLS Contiki library we tested using our [approach](https://icst2022.vrain.upv.es/details/icst-2022-papers/9/Applying-Symbolic-Execution-to-Test-Implementations-of-a-Network-Protocol-Against-its). To make experimenting with our approach easier, we have pre-installed all the necessary tools (e.g., KLEE and all SUTs) in an operating system. One can download the mentioned operating system alongside documentation from [here](https://zenodo.org/record/5929867#.YflNJ1vMLmH). 

# Compilation process
To build the test harness to be executed with KLEE, we need to issue the following command in `/sym-tinydtls/tests`:

 ```./compile.sh```

# KLEE command-line
We have executed klee using the following options:

```klee --simplify-sym-indices --write-cov -disable-inlining --optimize --use-forked-solver --use-cex-cache --libc=uclibc --external-calls=all --only-output-states-covering-new --max-memory-inhibit=false --search=random-path --search=nurs:covnew --max-memory=6000 --max-sym-array-size=4096 --max-solver-time=10s```


`-simplify-sym-indices:` Simplify symbolic accesses

`-write-cov:` Coverage information for each test case

`-disable-inlining:` Inline Expansion will not be carried output

`-optimize:` Optimize the code before execution

`-use-forked-solver:` Run the core SMT solver in a forked process

`-use-cex-cache:` Use the counterexample cache

`-libc=uclibc:` Link in uclibc (adapted for KLEE)

`-external-calls=all:`This concretizes any symbolic arguments in calls to external functions.

`-only-output-states-covering-new:` Only output test cases covering new code

`-max-memory-inhibit=false:` It does not inhibit forking at memory cap

`-search=random-path -search=nurs:covnew` use Random Path Selection interleaved with Non Uniform Random Search (NURS) with Coverage-New

`-max-sym-array-size=4096:` If a symbolic array exceeds this size (in bytes), symbolic addresses into this array are concretized. it is set according to the KLEE paper.

`-max-memory=32000` Refuse to fork when the amount of memory being used exceeds 32GB

`-max-solver-time=10s:` Maximum amount of time for a single SMT query

# Harness command-line

```./run.sh```

Type the rule when it asks for it.

# Conformance Bugs Found


### Epoch Server Rule 1:

- Status: *Violated*
- Number of paths: 3
- Witness Values for Epoch:
  - CH0: 257
  - CH2: 257
  - CKE: 0
  - CCC: 0


### Epoch Client Rule 1:

- Status: *Violated*
- Number of paths: 6
- Witness Values for Epoch:
  - HVR: 0
  - SH: 65280
  - SHD: 0
  - SCC: 0

### Record Sequence Number Uniqueness Server Rules 2:

- Status: *Violated*
- Number of paths: 1
- Witness Values for Record Sequence Number:
  - CH0: 1099511627776
  - CH2: 0
  - CKE: 0
  - CCC: 0
- Validation: successful

### Record Sequence Number Uniqueness Client Rules 2:

- Status: *Violated*
- Number of paths: 1
- Witness Values for Record Sequence Number:
  - HVR: 1099511627776
  - SH:  0
  - SHD: 0
  - SCC: 0
- Validation: pending

### Record Sequence Number Window Server Rules 3:

- Status: *Violated*
- Number of paths: 
- Witness Values for Record Sequence Number:
  - CH2: 65
  - CKE: 64
  - CCC: 0
- Validation: pending

### Record Sequence Number Window Client Rules 3:

- Status: *Violated*
- Number of paths: 
- Witness Values for Record Sequence Number:
  - HVR: 277
  - SH:  4
  - SHD: 64
  - SCC: 0
- Validation: pending

### Message Sequence Number Server Rule 4:

- Status: *Violated*
- Witness Values for Message Sequence Number:
  - CH0: 257
  - CH2: 111
  - CKE: 112
- Validation: pending

### Message Sequence Number Client Rule 4:

- Status: *Violated*
- Witness Values for Message Sequence Number:
  - HVR: 257
  - SH: 0
  - SHD: 1
- Validation: pending

### Fragment Offset Server Rule 5:

- Status: *Violated*
- Witness Values for Fragment Offset:
  - CH0: 65793
  - CH2: 16711937
  - CKE: 65793
- Validation: pending

### Fragment Offset Client Rule 5:

- Status: *Violated*
- Witness Values for Fragment Offset:
  - HVR: 65793
  - SH: 65793
  - SHD: 65793
- Validation: pending

### Fragment Length Message Length Equality Server Rule 6:

- Status: *Violated*
- Witness Values:
  - CH0.FL: 0
  - CH0.ML: 65536
  - CH2.FL: 0
  - CH2.ML: 0
  - CKE.FL: 0
  - CKE.FL: 0
- Validation: pending  

### Fragment Length Message Length Equality Client Rule 6:

- Status: *Violated*
- Witness Values:
  - HVR.FL: 0
  - HVR.ML: 65536
  - SH.FL: 0  
  - SH.ML: 0
  - SHD.FL: 0
  - SHD.ML: 0
- Validation: pending

### Fragment Length Record Length Relation Server Rule 7:

- Status: *Violated*
- Witness Values:
  - CH2.RL: 68
  - CH2.FL: 65332
  - CKE.RL: 29
  - CKE.FL: 65533

### Fragment Length Record Length Relation Client Rule 7:

- Status: *Violated*
- Witness Values:
  - HVR.RL: 15
  - HVR.FL: 65527
  - SH.RL: 50
  - SH.FL: 65526
  - SHD.RL: 0
  - SHD.FL: 0

### Message Length Record Length Relation Server Rule 8:

- Status: *Violated*
- Witness Values:
  - CH2.RL: 68
  - CH2.ML:64
  - CKE.RL: 29
  - CKE.ML: 0

### Message Length Record Length Relation Client Rule 8:

- Status: *Violated*
- Witness Values:
  - HVR.RL: 
  - HVR.ML: 
  - SH.RL: 
  - SH.ML: 
  - SHD.RL: 
  - SHD.ML: 

### Handshake Version Server Rule 10:

- Status: *Violated*
  - CH0: fe fd
  - CH2: ff fe
- Validation: failed, could not complete handshake with invalid CH. However, could complete a shortened handshake by excluding CH2. This still should not have been possible (since CH0 has bogus version). 
- Note: there is no handshake protocol version field in CKE

### Handshake Version Client Rule 10:

- Status: *Violated*
  - HVR: ff fe
  - SH:  fe fd
- Validation: successful
- Note: fefd corresponds to DTLS 1.2 (hence the SH message appears to be valid)

### Version Match Server Rule 11:

- Status: *Violated*
  - CH0.RV: ff fe
  - CH0.HV: 00 00
  - CH2.RV: fe fd
  - CH2.HV: 01 01
- Validation: successful, CH0 is ignored by TinyDTLS and only CH2 is processed, prompting a shortened handshake. As we have seen from rule 14, this handshake can be completed by utilizing bogus handshake version numbers.

### Version Match Client Rule 11:

- Status: *Violated*
  - HVR.RV: fe fd
  - HVR.HV: ff fe
  - SH.RV: fe fd
  - SH.HV: fe fd

### Session ID length Rule 13:

- Status: *Violated*
  - CH0: 1 
  - CH2: 0


# Memory Bugs Found


### 1

- Step: Client Hello
- Type: *Assertion Violation*
- Location: `dtls.c:974: dtls_update_parameters()`

### 2

- Step: Client Hello
- Type: *Buffer overflow*
- Location: `dtls.c:280: dtls_get_cookie()`

### 3

- Step: Client Hello
- Type: *Buffer overflow*
- Location: `dtls-numeric.h:93: dtls_uint16_to_int()`

### 4

- Step: Hello Verify Request
- Type: *Buffer overflow*
- Location: `dtls.c:2618: check_server_hello()`

### 5

- Step: Client Hello 2
- Type: *Buffer overflow*
- Location: `dtls-numeric.h:88: dtls_uint8_to_int()`

### 6

- Step: Server Hello Done
- Type: *Assertion Violation*
- Location: `dtls.c:2873: check_certificate_request()`

# Old TinyDTLS README
CONTENTS

This library contains functions and structures that can help
constructing a single-threaded UDP server with DTLS support in
C99. The following components are available:

* dtls
  Basic support for DTLS with pre-shared key mode.

* tests
  The subdirectory tests contains test programs that show how each
  component is used.

BUILDING

When using the code from the git repository, invoke make to build DTLS as a
shared library.
